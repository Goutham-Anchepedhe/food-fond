import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-review-and-schedule',
  templateUrl: './review-and-schedule.component.html',
  styleUrls: ['./review-and-schedule.component.css']
})
export class ReviewAndScheduleComponent implements OnInit {
  @Input() data: any;
  
  constructor() { }

  ngOnInit(): void {
  }

}
