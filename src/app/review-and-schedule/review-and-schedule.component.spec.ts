import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewAndScheduleComponent } from './review-and-schedule.component';

describe('ReviewAndScheduleComponent', () => {
  let component: ReviewAndScheduleComponent;
  let fixture: ComponentFixture<ReviewAndScheduleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReviewAndScheduleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewAndScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
