import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[checkoutHost]',
})
export class CheckoutHostDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}