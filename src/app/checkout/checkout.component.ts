import { Component, OnInit, ViewChild, ComponentFactoryResolver } from '@angular/core';
import { AdCheckoutComponent } from './addcheckout.component';
import { CheckoutService } from './checkout.service';
import { CheckoutHostDirective } from './checkouthost.directive';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  checkout = [{id:1, name:'Review & Schedule'}, 
    {id:2, name:'Select Address'}, {id:3, name:'Check Quote'}, {id:4, name:'Order Placed'}];

  @ViewChild(CheckoutHostDirective, {static: true}) checkoutHost: CheckoutHostDirective;

  selectedCheckoutId = 1;

  checkoutSteps: AdCheckoutComponent[];

  constructor(private checkoutService: CheckoutService, private componentFactoryResolver: ComponentFactoryResolver) { }

  ngOnInit(): void {
    this.checkoutSteps = this.checkoutService.getCheckout();
    this.loadComponent();
  }

  loadComponent() {
    const adComponent= this.checkoutSteps[0];

    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(adComponent.component);
    const viewContainerRef = this.checkoutHost.viewContainerRef;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent<AdCheckoutComponent>(componentFactory);
    componentRef.instance.data = adComponent.data;
  }

}
