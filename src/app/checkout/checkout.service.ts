import { Injectable } from '@angular/core';
import { AdCheckoutComponent } from './addcheckout.component';
import { ReviewAndScheduleComponent } from '../review-and-schedule/review-and-schedule.component';

@Injectable({
    providedIn: 'root'
  })
export class CheckoutService {
    getCheckout() {
        return [
            new AdCheckoutComponent(ReviewAndScheduleComponent, {products:[
                {name: 'Dry Fruit', image:'../../assets/Dry_Jamoon_COMPRESSED.jpg', noOfProduct:1},
                {name: 'Dry Jamoon', image:'../../assets/Dry_Jamoon_COMPRESSED.jpg', noOfProduct:1},
                {name: 'Champakali', image:'../../assets/Champakali_COMPRESSED.jpg', noOfProduct:1},
            ]})
        ]
    }

}