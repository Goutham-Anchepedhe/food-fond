import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { ReviewAndScheduleComponent } from './review-and-schedule/review-and-schedule.component';
import { SelectAddressComponent } from './select-address/select-address.component';
import { CheckQuoteComponent } from './check-quote/check-quote.component';
import { OrderPlacedComponent } from './order-placed/order-placed.component';
import { CheckoutService } from './checkout/checkout.service';
import { CheckoutHostDirective } from './checkout/checkouthost.directive';

@NgModule({
  declarations: [
    AppComponent,
    CheckoutComponent,
    ReviewAndScheduleComponent,
    SelectAddressComponent,
    CheckQuoteComponent,
    OrderPlacedComponent,
    CheckoutHostDirective,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [CheckoutService],
  
  bootstrap: [AppComponent]
})
export class AppModule { }
